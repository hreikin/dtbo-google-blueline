Minimal dtbo partition for Google Pixel 3 (google-blueline), originally taken from the AOSP kernel with all device nodes removed for the Google Pixel 3 XL (google-crosshatch) and then modified slightly for the Google Pixel 3 (google-blueline).

Used for the mainline kernel.

This requires dtc and mkdtboimg (from AOSP libufdt) to build.
